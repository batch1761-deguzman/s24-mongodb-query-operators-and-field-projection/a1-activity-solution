// Find users with letter "y" in their firstname or lastname
// show only their email and isAdmin properties/fields
db.users.find({$or:[{firstname:{$regex:"y",$options:"$i"}},{lastname:{$regex:"y",$options:"$i"}}]},{"_id":0,"email":1,"isAdmin":1})
// Find users with letter "e" in their firstname and is an admin.
// show only their email and isAdmin properties/field
db.users.find({$and:[{firstname:{$regex:"e",$options:"$i"}},{isAdmin:true}]},{"_id":0, "email":1,"isAdmin":1})
// Find products with letter x in its name and has a price greater than or equal to 50000
db.products.find({$and:[{name:{$regex:"x",$options:"$i"}},{price:{$gte:50000}}]})
// Update all products with price less than 2000 to inactive
db.products.updateMany({price:{$lte:2000}},{$set:{isActive:false}})
// Delete all products with price greater than 20000
db.products.deleteMany({price:{$gt:20000}})
